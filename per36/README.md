# CNN for NBA Basketball Predictions
This project will focus on utilizing NBA data from 2017-2020 in order to predict the outcome of a game. 

## Project Notebooks
You can find two different notebooks in the project that can be used.

1. Getting_the_datas_NBA.ipynb
    * Notebook for retrieving the data from basketball-reference.com
2. One_Source_NBA_CNN.ipynb
    * Notebook used in the development of the data retrieval, parsing, and buiding the model. (pre one file for the games data)
3. NBA_Predictor_Full_CNN.ipynb
    * A reorganized notebook with all the code from the development one into multiple functions. This will run with multiple years compared to the original one that just ran in one year of data.

## How does the CNN works
The way we stucture the data and build the model is by matchups. Each team we take the top 10 active players on each team and match them up. Then we see what percentage of the players time is spent on any of the positons the opposing player plays and we multiply the attributes against that percenage.

### Data
For data source we are utilizing basketball-reference.com, we have downloaded the outcomes of every game (and some playoffs?) for the campaigns of 1996-1997 to 2019-2020. The other two files of data we have are the players per 36 stats of the year. This may not be the right way, we would probably want the per 36 to that game in the season. And the play-by-play data that contains the percentage of play for each player per position. (#### year). We have also made a matchup matric for every year wih the 16 features we are looking at.

#### Basketball-Reference Original Data
- outcomes###.pickle: contains the games outcomes.
- pbp: Play by play data containing the % of playing time by position for each player.
- per36.csv: Per 36 stats for a season
- season####.pickle: contains the data for all the games. Ex. [ { 'home':[ ... ], 'away':[ ... ]} ]

#### Matchup Matrix Data
There is only one file containing all the matchup matrix for all the data. This file is called `season12_20.pickle`.

Data format is a dictionary the following way:
       
       {
           "images":images,
           "labels":labels, 
           'all_data': all_data
       }

Each of the values in the dictionary contains different formats.
- images: Machup matrix considered an image of 162x16 (9 players per team * 18 matchups (9+9) and 16 features). Every two rows is a matchup.
- labels: Array of 0 and 1 where 0 means home team won and 1 means away team won.
- all_data: Pandas DataFrame which is a cleaner version of pbp and per36 per year. Used for scalling the data. 

### Attributes
We may want to divide the data into offense and defense and try to have the same amount of columns for each. This was thought in order to have a pixel of offense, defense, and other and make the string work that way. (this was before I removed columns that have the * on them for less features). Maybe we can add them back in the future.

- **Defense** - ORB, DRB, STL, BLK, *PF
- **Offense** - PTS, AST, TOV, FT%, 3P%, 2P%
- **Other**   - *Weight, *Height, Position%, *MP


## Outcomes of the project
At the moment of this writting we have a model hitting around 64-65% which is definitely not great percentage. This has been done with over 20 seasons.


